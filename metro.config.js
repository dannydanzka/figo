const path = require('path');
const blacklist = require('metro-config/src/defaults/blacklist');

module.exports = {
resolver: {
    extraNodeModules: new Proxy({}, {
        get: (target, name) => path.join(process.cwd(), `node_modules/${name}`),
      }),
      blacklistRE: blacklist([
        /node_modules\/.*\/node_modules\/react-native\/.*/,
    ])
},
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: false,
      },
    }),
  },
};