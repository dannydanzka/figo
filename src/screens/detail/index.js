import React, { Component } from 'react'
import { View, Image, StyleSheet, Alert, TouchableOpacity } from 'react-native'
import { scale as s, verticalScale as vs } from 'react-native-size-matters'

import AppLayout from '../app-layout'
import { Text } from '../../general-components'
import MS from '../../styles/main'
import InitLoading from '../../general-components/components/init-loading'
import MapView, { Marker } from 'react-native-maps'
import Star from '../home/components/star'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Linking } from 'react-native'
import openMap from 'react-native-open-maps'
import Geolocation from '@react-native-community/geolocation'

class Detail extends Component {
  
    constructor(props){
        super(props)

        this.state = {
            data: [],
            loadingData: true
        };
    }

    componentDidMount = async () => {
        await this.setState({ data: this.props.navigation.getParam('item'), loadingData: true })
        /*
        await Geolocation.getCurrentPosition( async (position) => {
            const url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='+position.coords.latitude+','+position.coords.longitude+'&destinations=place_id:'+this.state.data.PlaceId+'key=AIzaSyD0FhpL-OsOS5FI0ZPzfVFfFQDgi8gNkK0';
            await fetch(url)
            .then((resp) => resp.json())
            .then( async (data) => {
                console.log(data)
                await this.setState({ loadingData: true }) 
            })
            .catch(function(error) {
                Alert.alert(
                    'Error',
                    `Ocurrio un error al intentar obtener datos`,
                    [{ text: 'OK', onPress: () => {}}],
                    { cancelable: false },
                )
            });

            console.log(url)
        },
        error => {
            console.log(error)
            
            Alert.alert(
                'Error',
                `Ocurrio un error al intentar obtener la ubicación, por favor vuelve a intentar. (Recuerda haber aceptado los permisos de ubicación)`,
                [{ text: 'OK', onPress: () => {}}],
                { cancelable: false },
            )
        },
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 });
        */
    }

    handleClickWebsite = () => {
        Linking.canOpenURL(this.state.data.Site).then(supported => {
            if (supported) {
                Linking.openURL(this.state.data.Site);
            } else {
                console.log("Don't know how to open URI: " + this.state.data.Site);
            }
        })
    }

    handleClickMap = () => {
        console.log(this.state)
        openMap({ latitude: this.state.data.Latitude, longitude: this.state.data.Longitude })
    }

    render() {
        const { loadingData, data } = this.state,
        { navigation } = this.props

        if( !loadingData )
            return <InitLoading />
        
        console.log(data)

        return (
            <AppLayout navigation={ navigation } isback={ true }>
                <View style={ styles.container } >
                    <View style={styles.containerMaps}>
                        <MapView
                            style={ styles.map }
                            region={{
                                latitude: data ? data.Latitude : 20.667079,
                                longitude: data ? data.Longitude : -103.414044,
                                latitudeDelta: 0.01,
                                longitudeDelta: 0.01
                            }}
                            showsUserLocation={ true }
                            followUserLocation={ true }
                        >
                            <Marker
                                title={ data ? data.PlaceName : 'Title' }
                                description={ data ? data.Address : 'Description' }
                                coordinate={{
                                    latitude: data ? data.Latitude : 20.667079,
                                    longitude: data ? data.Longitude : -103.414044
                                }}
                            >    
                                <Image source={ require('../../assets/images/pinSelected.png') } style={ styles.pin } />
                            </Marker>
                        </MapView>
                    </View>

                    <View style={ styles.containDescription } >
                        <View style={ styles.containerContent }>
                            <View>
                                <Text style={{ fontSize: s(22), color: MS.greenBlue }}>{ data.PlaceName }</Text>
                            </View>
                            <View style={ styles.containerstar }>
                                <Star rate={ data.Rating } />
                                <Text style={{ marginLeft: s(3), marginTop: s(-1) }} >({ data.Rating })</Text>
                            </View>
                            <View style={{ marginTop: s(10) }}>
                                <Text style={{ fontSize: s(17), color: MS.gunmetal }}>{ data.AddressLine1 }</Text>
                                <Text style={{ fontSize: s(17), color: MS.gunmetal }}>{ data.AddressLine2 }</Text>
                            </View>
                        </View>
                        <View style={ styles.containerDetail }>
                            <View style={ styles.containerDistance }>
                                <Text style={{ color: MS.grey2, fontSize: s(14) }}>{ (data.distance / 1000).toFixed(0) } Km</Text>
                            </View>
                            {
                                data.IsPetFriendly ?
                                <View style={ styles.containerPet } >
                                    <Image source={ require('../../assets/images/dogFriendlyActive.png') } style={ styles.pet } />
                                </View> :
                                <View style={ styles.containerPet } ></View>
                            }
                        </View>
                    </View>

                    <View style={ styles.containActions }>
                        <TouchableOpacity 
                            style={ styles.containerTab }
                            onPress={ this.handleClickMap }
                        >
                            <View style={ styles.containerImage }>
                                <Image source={ require('../../assets/images/icons8RouteFilled.png') } style={ styles.pet } />
                            </View>
                            <View style={ styles.containerDescription }>
                                <Text style={ styles.titleTab }>Directions</Text>
                                <Text style={ styles.subTitleTab }>15 min drive</Text>
                            </View>
                            <View style={ styles.containerIcon }>
                                <Icon name={'angle-right'} size={ s(30) } color={ MS.grey2 } />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={ styles.containerTab }
                            onPress={ () => { Linking.openURL(`tel:${ data.PhoneNumber }`) } }
                        >
                            <View style={ styles.containerImage }>
                                <Image source={ require('../../assets/images/cellIconsPhone.png') } style={ styles.call } />
                            </View>
                            <View style={ styles.containerDescription }>
                                <Text style={ styles.titleTab }>Call</Text>
                                <Text style={ styles.subTitleTab }>{ data.PhoneNumber }</Text>
                            </View>
                            <View style={ styles.containerIcon }>
                                <Icon name={'angle-right'} size={ s(30) } color={ MS.grey2 } />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={ styles.containerTab }
                            onPress={ this.handleClickWebsite }
                        >
                            <View style={ styles.containerImage }>
                                <Image source={ require('../../assets/images/cellIconsWebsite.png') } style={ styles.web } />
                            </View>
                            <View style={ styles.containerDescription }>
                                <Text style={ styles.titleTab }>Visit Website</Text>
                                <Text style={ styles.subTitleTab }>{ data.Site }</Text>
                            </View>
                            <View style={ styles.containerIcon }>
                                <Icon name={'angle-right'} size={ s(30) } color={ MS.grey2 } />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </AppLayout>
        )
    }
}

const defaultStyles = {
    container: {
        flex: 1,
    },
    background: {
        flex: 1,
        paddingHorizontal: s(30),
        resizeMode: 'contain',
    },
    containerMaps: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    map: {
        width: '100%',
        height: '100%'
    },
    pin: {
        width: s(45), 
        height: s(45), 
        resizeMode: 'contain' 
    },
    containDescription: {
        flex: 1,
        flexDirection: 'row', 
        borderBottomWidth: s(1), 
        borderBottomColor: MS.grey,
        paddingLeft: s(25)
    },
    containActions: {
        flex: 1.1,
    },
    containerContent: { 
        flex: 6, 
        paddingLeft: s(2), 
        justifyContent: 'center',
    },
    containerstar: { 
        height: vs(20), 
        marginTop: s(8), 
        marginLeft: s(-3),
        flexDirection: 'row'
    },
    containerDetail: { 
        flex: 2.2, 
        justifyContent: 'space-between', 
    },
    containerPet: { 
        flex: 1.5,
        justifyContent: 'flex-start', 
        alignItems: 'center',
        marginTop: s(5)
    },
    pet: {
        width: s(40),
        height: vs(40),
        resizeMode: 'contain',
    },
    web: {
        width: s(55),
        height: vs(55),
        resizeMode: 'contain',
    },
    call: {
        width: s(55),
        height: vs(55),
        resizeMode: 'contain',
    },
    containerDistance: { 
        flex: 1, 
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'flex-end',
    },
    containerTab: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: s(20),
        borderBottomWidth: s(1), 
        borderBottomColor: MS.grey,
    },
    containerImage: {
        flex: 1.5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    containerDescription: {
        flex: 5,
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingVertical: s(10)
    },
    titleTab: {
        fontSize: s(20),
        color: MS.gunmetal
    },
    subTitleTab: {
        fontSize: s(12),
        color: MS.grey1,
    },
    containerIcon: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
}

const styles = StyleSheet.create(defaultStyles);

export default Detail;

