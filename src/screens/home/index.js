import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, ActivityIndicator, FlatList, Image, Alert } from 'react-native'
import { scale as s, verticalScale as vs } from 'react-native-size-matters'
import AppLayout from '../app-layout'
import { InitLoading, Text } from '../../general-components'
import MS from '../../styles/main'
import Place from './components/place'
import getDistance from 'geolib/es/getDistance'
import getPreciseDistance from 'geolib/es/getPreciseDistance'
import Geolocation from '@react-native-community/geolocation'

class Home extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            loadingData: false
        };
    }

    componentDidMount = async () => {
        await Geolocation.getCurrentPosition( async (position) => {
            const url = 'http://www.mocky.io/v2/5bf3ce193100008900619966';
            await fetch(url)
            .then((resp) => resp.json())
            .then( async (data) => {
                const nPlaces = data.map(p => {
                    return {
                        Address: p.Address, 
                        AddressLine1: p.AddressLine1, 
                        AddressLine2: p.AddressLine2, 
                        Category: p.Category, 
                        IsOpenNow: p.IsOpenNow, 
                        IsPetFriendly: p.IsPetFriendly, 
                        Latitude: p.Latitude, 
                        Longitude: p.Longitude, 
                        PhoneNumber: p.PhoneNumber, 
                        PlaceId: p.PlaceId, 
                        PlaceName: p.PlaceName, 
                        Rating: p.Rating, 
                        Site: p.Site, 
                        Thumbnail: p.Thumbnail, 
                        distance: getPreciseDistance( position.coords, { latitude: p.Latitude, longitude: p.Longitude } )
                    } 
                });
                
                let sort = nPlaces.sort( (a, b) => {
                    if ( a.distance < b.distance ) return -1
                    if ( a.distance > b.distance ) return 1
                    return 0;
                });

                await this.setState({ data: sort, loadingData: true }) 
            })
            .catch(function(error) {
                Alert.alert(
                    'Error',
                    `Ocurrio un error al intentar obtener datos`,
                    [{ text: 'OK', onPress: () => {}}],
                    { cancelable: false },
                )
            });
        },
        error => {
            console.log(error)
            
            Alert.alert(
                'Error',
                `Ocurrio un error al intentar obtener la ubicación, por favor vuelve a intentar. (Recuerda haber aceptado los permisos de ubicación)`,
                [{ text: 'OK', onPress: () => {}}],
                { cancelable: false },
            )
        },
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 });
    }

    render() {
        const { data, loadingData } = this.state,
        { navigation } = this.props;

        if( !loadingData )
            return <InitLoading />

        return (
            <AppLayout>
                <View style={ styles.container } >
                    <FlatList
                        data={ data }
                        renderItem={({ item }) => ( <Place item={ item } navigation={ navigation } /> )}
                        keyExtractor={ item => item.PlaceId }
                        ListEmptyComponent={ <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Text>Sin datos</Text></View> }
                    />
                </View>
            </AppLayout>
        )
    }
}

const defaultStyles = {
    container: {
        flex: 1
    }
}

const styles = StyleSheet.create(defaultStyles)

export default Home

