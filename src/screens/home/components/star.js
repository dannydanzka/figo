import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import MS from '../../../styles/main'
import { scale as s, verticalScale as vs } from 'react-native-size-matters'
import Icon from 'react-native-vector-icons/FontAwesome'
import Icon5 from 'react-native-vector-icons/FontAwesome5'
import Stars from 'react-native-stars'

class Star extends Component {
    
    constructor(props){
        super(props)
    }
    
    render() {
        
        const { rate } = this.props
        
        return (
            <View style={ styles.container } >
            	<Stars
                    rating={ rate }
                    count={ 5 }
                    half={ true }
                    fullStar={ <Icon name={'star'} size={ s(16) } color={ MS.orange } style={{ marginLeft: s(2) }} /> }
                    emptyStar={ <Icon name={'star'} size={ s(16) } color={ MS.grey } style={{ marginLeft: s(2) }} /> }
                    halfStar={ <Icon5 name={'star-half-alt'} size={ s(16) } color={ MS.orange } style={{ marginLeft: s(2) }} /> }
                    disabled={ true }
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row'
    }
})

export default Star