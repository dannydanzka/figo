import React, { Component } from 'react'
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native'
import MS from '../../../styles/main'
import { scale as s, verticalScale as vs } from 'react-native-size-matters'
import { Text } from '../../../general-components'
import Star from './star'
import Icon from 'react-native-vector-icons/FontAwesome'

class Place extends Component {
    
    constructor(props){
        super(props)
    }
    
    render() {
        
        const { item, navigation } = this.props
        
        return (
            <View style={ styles.container } >
                <TouchableOpacity 
                    style={ styles.containerPlace }
                    onPress={ () => navigation.navigate('Detail', { item, latlng: { latitud: item.Latitud, longitud: item.longitud } },  )}
                >
                    <View style={ styles.containerImage }>
                        <Image style={ styles.image } source={{ uri: item.Thumbnail }} />
                    </View>
                    <View style={ styles.containerContent }>
                        <View>
                            <Text style={{ fontSize: s(16), color: MS.gunmetal }}>{ item.PlaceName }</Text>
                        </View>
                        <View style={ styles.containerstar }>
                            <Star rate={ item.Rating } />
                            <Text style={{ marginLeft: s(3), marginTop: s(-1) }} >({ item.Rating })</Text>
                        </View>
                        <View style={{ marginTop: s(6) }}>
                            <Text style={{ fontSize: s(13), color: MS.grey1 }}>{ item.Address }</Text>
                        </View>
                    </View>
                    <View style={ styles.containerDetail }>
                        <View style={ styles.containerDistance }>
                            <Text style={{ color: MS.grey2, fontSize: s(10) }}>{ (item.distance / 1000).toFixed(0) } Km</Text>
                            <Icon name={'angle-right'} size={ s(25) } color={ MS.grey1 } style={{ paddingLeft: s(10), marginTop: s(-2) }} />
                        </View>
                        {
                            item.IsPetFriendly ?
                            <View style={ styles.containerPet } >
                                <Image source={ require('../../../assets/images/dogFriendlyActive.png') } style={ styles.pet } />
                                <Text style={{ fontSize: s(12), color: MS.grey2, marginTop: s(3) }}>Pet Friendly</Text>
                            </View> :
                            <View style={ styles.containerPet } ></View>
                        }
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent'
    },
    containerPlace: { 
        height: vs(110), 
        width: '100%', 
        flexDirection: 'row', 
        borderBottomWidth: s(1), 
        borderBottomColor: MS.grey 
    },
    containerImage: { 
        flex: 2.3, 
        alignItems: 'center' 
    },
    image: { 
        width: s(60), 
        height: s(60), 
        borderRadius: s(5), 
        marginTop: s(10) 
    },
    containerContent: { 
        flex: 6, 
        paddingLeft: s(2), 
        justifyContent: 'center',
    },
    containerstar: { 
        height: vs(20), 
        marginTop: s(3), 
        marginLeft: s(-3),
        flexDirection: 'row'
    },
    containerDetail: { 
        flex: 2.2, 
        justifyContent: 'space-between', 
    },
    containerPet: { 
        flex: 1.5,
        justifyContent: 'flex-start', 
        alignItems: 'center',
    },
    pet: {
        width: s(30),
        height: vs(30),
        resizeMode: 'contain',
    },
    containerDistance: { 
        flex: 1, 
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'center',
    }
})

export default Place
