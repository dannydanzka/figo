import React, { Component } from 'react'
import { View, StyleSheet, SafeAreaView, Image, TouchableOpacity } from 'react-native'
import { scale as s, verticalScale as vs } from 'react-native-size-matters'
import MS from '../../styles/main'
import AppState from '../../app-state'
import { Loader, Text } from '../../general-components'
import Icon from 'react-native-vector-icons/FontAwesome'

class AppLayout extends Component {
    
    constructor(props){
        super(props)
    }
    
    render() {
        
        const { isback, navigation } = this.props

        return(
            <AppState>
                <SafeAreaView style={{ flex: 1 }} >
                    {
                        isback ? 
                        <View style={[ styles.header, { flexDirection: 'row' }] } >
                            <TouchableOpacity 
                                style={ styles.iconContainer }
                                onPress={ () => navigation.navigate('Home') }
                            >
                                <Icon name={'angle-left'} size={ s(40) } color={ MS.dark } style={{ paddingLeft: s(10), marginTop: s(-2) }} />
                            </TouchableOpacity>
                            <View style={ styles.titleContainer }>
                                <Text style={{ fontSize: s(18), letterSpacing: 2 }}>DETAIL</Text>
                            </View>
                            <View style={{ flex: 1 }}></View>
                        </View> :
                        <View style={ styles.header } >
                            <Image source={ require('../../assets/images/figoLogo.png') } style={ styles.logo } />
                        </View>
                    }
                    <View style={ styles.content }>
                        { this.props.children }
                    </View>
                </SafeAreaView>
            </AppState>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: MS.white
    },
    logo: {
        width: s(90),
        height: vs(40),
        resizeMode: 'contain',
    },
    header: { 
        height: vs(65), 
        backgroundColor: 'rgba(255, 255, 255, 1)', 
        alignItems: 'center', 
        justifyContent: 'center',
        borderBottomWidth: s(1),
        borderBottomColor: 'rgba(223, 223, 225, 1)'
    },
    iconContainer: { 
        flex: 1, 
        justifyContent: 'flex-start', 
        alignItems: 'flex-start'  
    },
    titleContainer: { 
        flex: 2, 
        justifyContent: 'center', 
        alignItems: 'center', 
    }
});

export default AppLayout

