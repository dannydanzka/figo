import InitLoading from './components/init-loading'
import Loader from './components/loader'
import Text from './components/text'

export { InitLoading, Loader, Text }