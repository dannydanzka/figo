import React, { Component } from 'react'
import { View, StyleSheet, Image, ActivityIndicator, Text } from 'react-native'
import MS from '../../styles/main'
import { scale as s, verticalScale as vs } from 'react-native-size-matters'

class InitLoading extends Component {

    constructor(props){
        super(props)
    }
    
    render() {
        return(
            <View style={ styles.container } >
                <Image source={ require('../../assets/images/figoLogo2x.png') } style={ styles.logo } />
                <View style={ styles.loadingContainer } >
                    <ActivityIndicator size="large" color={ MS.tealish } />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: s(180),
        height: vs(150),
        resizeMode: 'contain'
    },
    loadingContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },
})

export default InitLoading;