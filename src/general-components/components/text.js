/**
 * @author Ernesto on 08/08/2018
 * Se utiliza este componente para mantener un tipo de fuente en toda la App.
 */
import React from 'react'
import { Text as ReactText, StyleSheet } from 'react-native'
import MS from '../../styles/main'

function Text(props){
  return(
    <ReactText {...props} style={[styles.text, props.style || {}]}>{props.children}</ReactText>
  )
}

const styles = StyleSheet.create({
    text: {
        fontFamily: MS.fontFamilyApp,
        color: MS.whiteText
    }
})

export default Text