import React from'react'
import {
  View,
  ActivityIndicator,
} from'react-native'
import { Overlay } from 'react-native-elements'
import MS from '../../styles/main'

const Loader = props => (
  <Overlay
    isVisible={true}
    width="auto"
    height="auto"
    overlayStyle={{backgroundColor: 'transparent', elevation: 0 }}
    onBackdropPress={() => { false }}
  >
    <ActivityIndicator size="large" color={ MS.tealish }/>
  </Overlay>
)

export default Loader