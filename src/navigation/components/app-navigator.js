import React from 'react'
import { createStackNavigator, createSwitchNavigator } from 'react-navigation'

import Home from '../../screens/home'
import Detail from '../../screens/detail'

const AppNavigator = createSwitchNavigator(
    {
        Main: { screen: createStackNavigator({ Home, Detail }, { headerMode: 'Main' }) },
    },
    {
        initialRouteName: 'Main',
        backBehavior: 'initialRoute',
        headerMode: 'none'
    }
)

export default AppNavigator