/**
 * @author Roberto Ramírez
 */
import React, { Component } from 'react';
import { AppState as AppStateReact } from 'react-native';
import NavigationService from './navigation/components/navigation-service';

/**
 * Detecta cuando la App se abre, se cierra y se navega.
 * Esta clase puede ser usada para otros própositos también.
 */
class AppState extends Component {

    state = {
        appState: AppStateReact.currentState
    }

    componentDidMount() {
        AppStateReact.removeEventListener('change', this._handleAppStateChange);
        AppStateReact.addEventListener('change', this._handleAppStateChange);
    }

    componentWillUnmount() {
        AppStateReact.removeEventListener('change', this._handleAppStateChange);
    }

    /*
    * IMPORTANTE: Si se detecta que el método se ejecutando mútiples veces entonces reinstalar la App.
    */
    _handleAppStateChange = async (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {}
        this.setState({ appState: nextAppState }, state => {});
    }

    render() {
        return (
            this.props.children 
        );
    }
}

export default AppState;