/**
 * @author Ernesto 
 */
import moment from 'moment';
import 'moment/locale/es';

moment.locale('es');

moment.defineLocale('es', {
    monthsShortDot: 'Ene_Feb_Mar_Abr_May_Jun_Jul_Ago_Sep_Oct_Nov_Dic.'.split('_'),
    monthsShort: 'Ene_Feb_Mar_Abr_May_Jun_Jul_Ago_Sep_Oct_Nov_Dic'.split('_'),
    months:  'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_')
});

export default moment;