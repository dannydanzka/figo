/**
 * Combinación de todos los reducers
 */

import { combineReducers } from 'redux'
import map from './map'

const reducers = combineReducers({ map })

export default reducers