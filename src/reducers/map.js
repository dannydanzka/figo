import _ from 'lodash'

const initialState = {
    data: null,
    isFetching: false,
}

function map(state = initialState, action){
  let cState = _.cloneDeep(state)

  switch(action.type){
    case 'SET_MAP_INFO': {
      cState = {...cState, ...action.payload}
      return cState
    }
    case 'RESET_MAP_STATE': {
      return initialState
    }
  }

  return state
}

export default map