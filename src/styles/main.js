import { scale as s, verticalScale as vs } from 'react-native-size-matters'

const MS = {
    white: 'rgba(255, 255, 255, 1)',
    orange: 'rgba(255, 163, 0, 1)',
    grey: 'rgba(223, 223, 225, 1)',
    greenBlue: 'rgba(44, 96, 133, 1)',
    green: 'rgba(119, 208, 206, 1)',
    grey1: 'rgba(127, 136, 143, 1)',
    gunmetal: 'rgba(87, 91, 94, 1)',
    tealish: 'rgba(47, 179, 180, 1)',
    grey2: 'rgba(140, 150, 157, 1)',
    green2: 'rgba(108, 211, 205, 1)',
    transparent: 'rgba(255, 255, 255, 0)',
    overlay: 'rgba(140, 150, 157, 0.5)',
    green3: 'rgba(115, 218, 210, 1)',
    dark: 'rgba(32, 32, 32, 1)',
    black: 'rgba(3, 3, 3, 1)'
}

export default MS;