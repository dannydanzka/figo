/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from './src/store'
import InitLoading from './src/general-components/components/init-loading'
import AppNavigatorState from './src/navigation/containers/app-navigator-state'

type Props = {};

export default class App extends Component<Props> {
  render() {
    console.disableYellowBox = true;

    return (
      <Provider store={ store }>
          <PersistGate
            persistor={ persistor }
            loading={<InitLoading></InitLoading>}
          >
            <AppNavigatorState />    
          </PersistGate>
      </Provider>
    )
  }
}